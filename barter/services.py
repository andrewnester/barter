import pika
import json

class JobManager(object):        
    
    connection = None
    channel = None
        
    def __init__(self):
        if JobManager.connection is None:
            JobManager.connection = pika.BlockingConnection()
            JobManager.channel = JobManager.connection.channel()
            JobManager.channel.exchange_declare(exchange='commands',type='direct')
    
    def set_send_email_task(self, subject, msg, emails):
        self.__set_task__('mails', json.dumps({'subject': subject, 'msg': msg, 'emails': emails}))
        
    def __set_task__(self, routing_key, body):        
        JobManager.channel.basic_publish(exchange='commands',
                      routing_key=routing_key,
                      body=body)