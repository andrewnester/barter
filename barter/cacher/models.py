from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from postman.models import Message, MessageManager
from cacheops import invalidate_model

from barter.offers.models import ServiceOffer
from barter.privatemsgs.signals import message_updated

@receiver(post_save, sender=User)
def user_updated(sender, created, **kwargs):
    invalidate_model(ServiceOffer)

def message_updated_callback(sender, **kwargs):
    invalidate_model(Message)

message_updated.connect(message_updated_callback)