from postman.views import WriteView as BaseWriteView
from postman.views import ArchiveView as BaseArchiveView
from postman.views import DeleteView as BaseDeleteView
from postman.views import UndeleteView as BaseUndeleteView
from postman.views import MessageView as BaseMessageView
from postman.views import ConversationView as BaseConversationView

from barter.offers.decorators import only_has_accepted_offers
from django.utils.translation import ugettext_lazy as _

from .signals import message_updated

class GetMessageUpdateMixin(object):
    def get(self, request, *args, **kwargs):
        result = super(GetMessageUpdateMixin, self).get(request, *args, **kwargs)
        message_updated.send(sender=self.__class__)
        return result

class PostMessageUpdateMixin(object):
    def post(self, request, *args, **kwargs):
        result = super(PostMessageUpdateMixin, self).post(request, *args, **kwargs)
        message_updated.send(sender=self.__class__)
        return result

class WriteView(BaseWriteView):    
   
    @only_has_accepted_offers 
    def dispatch(self, *args, **kwargs):       
        return super(WriteView, self).dispatch(*args, **kwargs)
    
    def get_initial(self):
        initial = super(WriteView, self).get_initial()
        if self.request.method == 'GET':
            initial['subject'] = self.kwargs.get('subject', _('Offer Discussion'))
            
        return initial
    
class ArchiveView(PostMessageUpdateMixin, BaseArchiveView):
    pass
    
class DeleteView(PostMessageUpdateMixin, BaseDeleteView):
    pass
    
class UndeleteView(PostMessageUpdateMixin, BaseUndeleteView):
    pass
    
class MessageView(GetMessageUpdateMixin, BaseMessageView):
    pass

class ConversationView(GetMessageUpdateMixin, BaseConversationView):
    pass