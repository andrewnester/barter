from django.conf.urls import include, url

from .views import WriteView, MessageView, ConversationView, ArchiveView, DeleteView, UndeleteView
from .forms import WriteForm

urlpatterns = [
    url(r'^write/(?P<recipients>[^/#]+)/(?P<subject>[^#?]*)$',
        WriteView.as_view(form_classes=(WriteForm, WriteForm), success_url='postman_inbox'),
        name='postman_write'),
    url(r'^view/(?P<message_id>[\d]+)/$', MessageView.as_view(), name='postman_view'),
    url(r'^view/t/(?P<thread_id>[\d]+)/$', ConversationView.as_view(), name='postman_view_conversation'),
    url(r'^archive/$', ArchiveView.as_view(), name='postman_archive'),
    url(r'^delete/$', DeleteView.as_view(), name='postman_delete'),
    url(r'^undelete/$', UndeleteView.as_view(), name='postman_undelete'),
    url(r'^', include('postman.urls')),
]