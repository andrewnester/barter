from postman.forms import WriteForm as BaseWriteForm
from barter.forms import DivErrorListMixin
from django.forms.widgets import TextInput, Textarea
from django.forms.fields import CharField
from django.utils.translation import ugettext_lazy as _
from postman.fields import CommaSeparatedUserField

class WriteForm(DivErrorListMixin, BaseWriteForm):
    recipients = CommaSeparatedUserField(label=(_("Recipients"), _("Recipient")), help_text='', widget=TextInput(attrs={'class':'form-control', 'readonly': 'readonly'}))
    subject = CharField(label=_("Subject"), widget=TextInput(attrs={'class':'form-control', 'placeholder': _('Subject'), 'readonly': 'readonly'}))
    body = CharField(label=_("Message"), widget=Textarea(attrs={'class':'form-control', 'placeholder': _('Message')}))