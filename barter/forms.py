from barter.users.forms import DivErrorList

class DivErrorListMixin(object):
    def __init__(self, *args, **kwargs):
        kwargs_new = {'error_class': DivErrorList}
        kwargs_new.update(kwargs)
        super(DivErrorListMixin, self).__init__(*args, **kwargs_new)   