from django.contrib import admin
from .models import ServiceOffer, ServiceOfferResponse, ServiceOfferCategory

class ServiceOfferAdmin(admin.ModelAdmin):
    list_display = ('user', 'category', 'short', 'status', 'offer', 'date')

class ServiceOfferResponseAdmin(admin.ModelAdmin):
    list_display = ('user', 'offer', 'status', 'date')

class ServiceOfferCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias', 'parent') 

admin.site.register(ServiceOffer, ServiceOfferAdmin)
admin.site.register(ServiceOfferResponse, ServiceOfferResponseAdmin)
admin.site.register(ServiceOfferCategory, ServiceOfferCategoryAdmin)