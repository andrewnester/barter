from django.db.models import Model, CharField, TextField, DateTimeField, DecimalField, IntegerField, ForeignKey, Count
from django.contrib.auth.models import User


class ServiceOfferCategory(Model):

    categories = None

    name = CharField(max_length=255)
    alias = CharField(max_length=255)
    parent = ForeignKey(
        'self', related_name='children', null=True, blank=True)

    def __str__(self):

        return '%s' % (self.name)

    def __unicode__(self):
        return '%s' % (self.name)

    @staticmethod
    def get_children_ids(parent_id):
        if ServiceOfferCategory.categories is None:
            ServiceOfferCategory.categories = ServiceOfferCategory.objects.all()

        ids = [parent_id]
        for category in ServiceOfferCategory.categories:
            if category.parent_id == parent_id:
                ids.append(category.id)
                ids += ServiceOfferCategory.get_children_ids(category.id)

        return ids

    @staticmethod
    def get_root_categories():
        return ServiceOfferCategory.objects.filter(parent_id=None).all()


class ServiceOffer(Model):

    """

    Model class represents offer of some service

    """

    PENDING = 0
    ACTIVE = 1
    ACCEPTED = 2
    COMPLETED = 3
    CANCELLED = 4

    OFFER_STATUSES_CHOICES = (
        (PENDING, 'Pending'),
        (ACTIVE, 'Active'),
        (ACCEPTED, 'Accepted'),
        (COMPLETED, 'Completed'),
        (CANCELLED, 'Cancelled')
    )

    user = ForeignKey(User)
    category = ForeignKey(ServiceOfferCategory, null=True)
    offer = TextField()
    short = CharField(max_length=255)

    lat = DecimalField(
        max_digits=18, decimal_places=15, null=True, blank=True)
    lng = DecimalField(
        max_digits=18, decimal_places=15, null=True, blank=True)

    date = DateTimeField(auto_now=True)
    status = IntegerField(
        choices=OFFER_STATUSES_CHOICES, default=PENDING)

    update_time = DateTimeField(auto_now_add=True)

    @staticmethod
    def find_all(user_id, status):
        offers_set = ServiceOffer.objects.prefetch_related('user').filter(user_id=user_id)
        if status is not None:
            status_id = None
            for option in ServiceOffer.OFFER_STATUSES_CHOICES:
                if option[1].lower() == status:
                    status_id = option[0]

            offers_set = offers_set.filter(status=status_id)

        return offers_set.order_by('-update_time').all()

    @staticmethod
    def find_all_active(filters={}):
        offers_set = ServiceOffer.objects.prefetch_related('user').select_related(
            'user__profile').exclude(status__in=[ServiceOffer.PENDING, ServiceOffer.CANCELLED])

        offers_set = __apply_filters__(offers_set, filters)
        return offers_set.order_by('-update_time').all()

    @staticmethod
    def find_all_waiting_for_response(user_id):
        return ServiceOffer.objects.filter(
            serviceofferresponse__status=ServiceOfferResponse.WAITING).filter(
            user_id=user_id).order_by('-update_time').all()

    @staticmethod
    def get_matched_categories(filters={}):
        offers_set = ServiceOffer.objects.prefetch_related('category').exclude(
            status__in=[ServiceOffer.PENDING, ServiceOffer.CANCELLED])

        offers_set = __apply_filters__(offers_set, filters)
        return offers_set.values('category__alias', 'category__parent_id', 'category__name', 'category_id').annotate(total=Count('category_id')).all()


def __apply_filters__(offers_set, filters):

    if 'category_ids' in filters and filters['category_ids']:
        offers_set = offers_set.filter(category_id__in=filters['category_ids'])

    if 'range' in filters and filters['range']:
        left_top = filters['range'][0].split(';')
        right_bottom = filters['range'][1].split(';')

        offers_set = offers_set.filter(
            lng__gte=right_bottom[0]).filter(lng__lte=left_top[0])

        offers_set = offers_set.filter(
            lat__gte=left_top[1]).filter(lat__lte=right_bottom[1])

    if 'term' in filters and len(filters['term']) >= 3:
        offers_set = offers_set.filter(short__contains=filters['term'])

    return offers_set


class ServiceOfferResponse(Model):

    """

    Model class represents response to some offer

    """

    WAITING = 1
    ACCEPTED = 2
    DECLINED = 3

    RESPONSE_STATUSES_CHOICES = (
        (WAITING, 'Waiting'),
        (ACCEPTED, 'Accepted'),
        (DECLINED, 'Declined')
    )

    offer = ForeignKey(ServiceOffer)
    user = ForeignKey(User)
    reply = TextField(default='')
    date = DateTimeField(auto_now=True)
    status = IntegerField(
        choices=RESPONSE_STATUSES_CHOICES, default=WAITING)

    @staticmethod
    def find_all(user_id, status):
        responses_set = ServiceOfferResponse.objects.prefetch_related('offer').prefetch_related('offer__user__profile').filter(user_id=user_id)
        if status is not None:
            status_id = None
            for option in ServiceOfferResponse.RESPONSE_STATUSES_CHOICES:
                if option[1].lower() == status:
                    status_id = option[0]

            responses_set = responses_set.filter(status=status_id)

        return responses_set.all()

    @staticmethod
    def find_all_for_offer(offer_id, filters):
        items_set = ServiceOfferResponse.objects.prefetch_related(
            'user').select_related('user__profile').filter(offer_id=offer_id)
        if 'status' in filters and filters['status'] != '0':
            items_set = items_set.filter(status=filters['status'])

        return items_set.all()
