# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0014_serviceoffer_update_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceoffer',
            name='update_time',
            field=models.DateTimeField(),
        ),
    ]
