# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0012_auto_20150429_1212'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceoffercategory',
            name='alias',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
