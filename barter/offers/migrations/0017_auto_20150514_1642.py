# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0016_auto_20150514_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceoffer',
            name='update_time',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
