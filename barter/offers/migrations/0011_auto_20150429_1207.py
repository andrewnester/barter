# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0010_auto_20150429_1201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceoffercategory',
            name='parent',
            field=models.ForeignKey(related_name='children', to='offers.ServiceOfferCategory', null=True),
        ),
    ]
