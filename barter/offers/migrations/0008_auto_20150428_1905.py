# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0007_serviceofferresponse'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='serviceoffer',
            name='wish',
        ),
        migrations.AlterField(
            model_name='serviceofferresponse',
            name='status',
            field=models.IntegerField(default=1, choices=[(1, b'Waiting'), (2, b'Accepted'), (3, b'Declined')]),
        ),
    ]
