# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('offers', '0004_auto_20150427_1347'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceOffer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('wish', models.TextField()),
                ('offer', models.TextField()),
                ('date', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=0, choices=[(0, b'Pending'), (1, b'Active'), (2, b'Waiting'), (3, b'Accepted'), (4, b'Completed'), (5, b'Cancelled')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='offer',
            name='user',
        ),
        migrations.DeleteModel(
            name='Offer',
        ),
    ]
