# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0013_serviceoffercategory_alias'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceoffer',
            name='update_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 30, 13, 57, 16, 132140, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
