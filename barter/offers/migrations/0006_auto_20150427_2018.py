# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0005_auto_20150427_2000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceoffer',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'Pending'), (1, b'Active'), (2, b'Accepted'), (3, b'Completed'), (4, b'Cancelled')]),
        ),
    ]
