# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='status',
            field=models.IntegerField(default=1, choices=[(1, b'Active'), (2, b'Waiting'), (3, b'Accepted'), (4, b'Completed'), (5, b'Cancelled')]),
        ),
    ]
