# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0015_auto_20150514_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceoffer',
            name='lat',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='serviceoffer',
            name='lng',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
