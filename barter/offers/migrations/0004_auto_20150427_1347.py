# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0003_auto_20150424_1013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'Pending'), (1, b'Active'), (2, b'Waiting'), (3, b'Accepted'), (4, b'Completed'), (5, b'Cancelled')]),
        ),
    ]
