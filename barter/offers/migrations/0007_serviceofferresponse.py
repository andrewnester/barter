# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('offers', '0006_auto_20150427_2018'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceOfferResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reply', models.TextField(default=b'')),
                ('date', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=2, choices=[(1, b'Waiting'), (2, b'Accepted'), (3, b'Declined')])),
                ('offer', models.ForeignKey(to='offers.ServiceOffer')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
