# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0017_auto_20150514_1642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceoffer',
            name='lat',
            field=models.DecimalField(null=True, max_digits=18, decimal_places=15, blank=True),
        ),
        migrations.AlterField(
            model_name='serviceoffer',
            name='lng',
            field=models.DecimalField(null=True, max_digits=18, decimal_places=15, blank=True),
        ),
    ]
