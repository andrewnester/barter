# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0009_serviceoffer_short'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceOfferCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('parent', models.ForeignKey(related_name='children', to='offers.ServiceOfferCategory')),
            ],
        ),
        migrations.AddField(
            model_name='serviceoffer',
            name='category',
            field=models.ForeignKey(to='offers.ServiceOfferCategory', null=True),
        ),
    ]
