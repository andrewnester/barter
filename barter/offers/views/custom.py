from django.http.response import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.utils import timezone

from barter.users.decorators import need_filled_account, only_author

from barter.offers.forms import ServiceOfferResponseForm
from barter.offers.models import ServiceOffer, ServiceOfferResponse


@need_filled_account
def response(request, offer_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    form = ServiceOfferResponseForm

    if request.method == 'POST':
        form = ServiceOfferResponseForm(request.POST)
        if form.is_valid():
            offer_response = ServiceOfferResponse(
                user=request.user, offer=offer, reply=form.cleaned_data['reply'])
            offer_response.save()
            return HttpResponseRedirect(reverse('response_offer', args=(offer_id,)))

    response = ServiceOfferResponse.objects.filter(
        offer_id=offer_id, user_id=request.user.id).first()
    template = loader.get_template('offers/response.html')
    context = RequestContext(request, {
        'offer': offer,
        'response': response,
        'form': form,
        'current_user': request.user
    })
    return HttpResponse(template.render(context))


@only_author(ServiceOffer)
def accept_response(request, offer_id, response_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    offer_response = get_object_or_404(ServiceOfferResponse, pk=response_id)

    offer.status = ServiceOffer.ACCEPTED
    offer_response.status = ServiceOfferResponse.ACCEPTED

    if offer_response.offer_id != offer.id:
        return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))

    offer.save()
    offer_response.save()

    return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))


@only_author(ServiceOffer)
def decline_response(request, offer_id, response_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    offer_response = get_object_or_404(ServiceOfferResponse, pk=response_id)

    if offer_response.offer_id != offer.id:
        return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))

    offer_response.status = ServiceOfferResponse.DECLINED
    offer_response.save()

    return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))


@only_author(ServiceOffer)
def cancel(request, offer_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    offer.status = ServiceOffer.CANCELLED
    offer.save()
    return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))


@only_author(ServiceOffer)
def restore(request, offer_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    offer.status = ServiceOffer.ACTIVE
    offer.save()
    return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))


@only_author(ServiceOffer)
def up(request, offer_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    offer.update_time = timezone.now()
    offer.save()

    return HttpResponseRedirect(reverse('view_offer', args=(offer_id,)))