from datetime import timedelta

from django.http.response import HttpResponse
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.views.generic.edit import UpdateView, CreateView
from django.forms.models import inlineformset_factory

from barter.users.decorators import only_author, generic_need_filled_account, generic_only_author
from barter.settings import MEDIA_URL
from barter.uploader.models import Attachment

from barter.offers.forms import ServiceOfferForm, ServiceOfferResponseForm
from barter.offers.models import ServiceOffer, ServiceOfferResponse
from barter.offers.services import get_category_tree


class OfferCreateView(CreateView):
    model = ServiceOffer
    template_name_suffix = '_create_form'
    form_class = ServiceOfferForm

    @generic_need_filled_account
    def get(self, request, *args, **kwargs):
        return super(OfferCreateView, self).get(request, *args, **kwargs)

    @generic_need_filled_account
    def post(self, request, *args, **kwargs):
        return super(OfferCreateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        result = super(OfferCreateView, self).form_valid(form)

        FileFormSet = inlineformset_factory(
            ServiceOffer, Attachment, fields=('file',))
        formset = FileFormSet(
            self.request.POST, self.request.FILES, instance=form.instance)
        if formset.is_valid():
            formset.save()

        return result

    def get_success_url(self):
        return reverse('view_offer', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(OfferCreateView, self).get_context_data(**kwargs)
        context['categories'] = get_category_tree()

        FileFormSet = inlineformset_factory(
            ServiceOffer, Attachment, fields=('file',))
        context['file_formset'] = FileFormSet()

        return context


class OfferUpdateView(UpdateView):
    model = ServiceOffer
    template_name_suffix = '_update_form'
    form_class = ServiceOfferForm

    @generic_only_author(ServiceOffer)
    def get(self, request, *args, **kwargs):
        return super(OfferUpdateView, self).get(request, *args, **kwargs)

    @generic_only_author(ServiceOffer)
    def post(self, request, *args, **kwargs):
        return super(OfferUpdateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        result = super(OfferUpdateView, self).form_valid(form)

        FileFormSet = inlineformset_factory(
            ServiceOffer, Attachment, fields=('file',))
        formset = FileFormSet(
            self.request.POST, self.request.FILES, instance=self.object)
        if formset.is_valid():
            formset.save()

        return result

    def get_success_url(self):
        return reverse('view_offer', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(OfferUpdateView, self).get_context_data(**kwargs)
        context['categories'] = get_category_tree()

        FileFormSet = inlineformset_factory(
            ServiceOffer, Attachment, fields=('file',))
        context['file_formset'] = FileFormSet(instance=self.object)

        return context


@only_author(ServiceOffer)
def view(request, offer_id):
    offer = get_object_or_404(ServiceOffer, pk=offer_id)
    form = ServiceOfferResponseForm

    filters = request.GET

    responses_set = ServiceOfferResponse.find_all_for_offer(offer_id, filters)

    paginator = Paginator(responses_set, 10)
    page = request.GET.get('page')

    try:
        responses = paginator.page(page)
    except PageNotAnInteger:
        responses = paginator.page(1)
    except EmptyPage:
        responses = paginator.page(paginator.num_pages)

    update_time = offer.update_time + timedelta(days=1)
    update_time_delta = update_time - timezone.now()

    template = loader.get_template('offers/view.html')
    context = RequestContext(request, {
        'MEDIA_URL': MEDIA_URL,
        'offer': offer,
        'attachments': offer.attachment_set.all(),
        'responses': responses,
        'form': form,
        'current_user': request.user,
        'statuses': ServiceOfferResponse._meta.get_field('status').choices,
        'filters': filters,
        'update_hours_delta': update_time_delta.days*24 + update_time_delta.seconds//3600
    })
    return HttpResponse(template.render(context))
