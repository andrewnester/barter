from django.http.response import HttpResponse
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404

from barter.offers.models import ServiceOffer, ServiceOfferCategory, ServiceOfferResponse
from barter.offers.widgets import SearchFieldWidget


def categories(request):
    template = loader.get_template('offers/categories.html')
    context = RequestContext(request, {
        'categories': ServiceOfferCategory.get_root_categories()
    })
    return HttpResponse(template.render(context))


def offer_list(request, category=None):
    if category is not None:
        category = ServiceOfferCategory.objects.filter(alias=category).first()
        if category is None:
            raise Http404

    filters = {}
    filters['category_ids'] = []
    if category is not None:
        filters['category_ids'] = ServiceOfferCategory.get_children_ids(
            category.id)

    filters['term'] = request.GET.get('term', '')
    filters['range'] = request.GET.getlist(
        'range', ['33.7;50', '22.3;57.7', '5'])

    # get list of matched categories without filtering by selected categories
    matched_categories = ServiceOffer.get_matched_categories(filters=filters)

    # add filter of selected categories
    checked_categories = request.GET.getlist('category_id', [])
    if checked_categories:
        filters['category_ids'] = checked_categories

    offers_set = ServiceOffer.find_all_active(filters=filters)

    paginator = Paginator(offers_set, 10)
    page = request.GET.get('page')

    try:
        offers = paginator.page(page)
    except PageNotAnInteger:
        offers = paginator.page(1)
    except EmptyPage:
        offers = paginator.page(paginator.num_pages)

    widget = SearchFieldWidget()

    template = loader.get_template('offers/list.html')
    context = RequestContext(request, {
        'filters': filters,
        'search_widget': widget.render(name="search-field", value=filters['term']),
        'category': category,
        'categories': matched_categories,
        'offers': offers,
        'current_user': request.user
    })
    return HttpResponse(template.render(context))


def my_offers(request, status=None):
    offers_set = ServiceOffer.find_all(user_id=request.user.id, status=status)

    paginator = Paginator(offers_set, 10)
    page = request.GET.get('page')

    try:
        offers = paginator.page(page)
    except PageNotAnInteger:
        offers = paginator.page(1)
    except EmptyPage:
        offers = paginator.page(paginator.num_pages)

    template = loader.get_template('offers/my.html')
    context = RequestContext(request, {
        'offers': offers,
    })
    return HttpResponse(template.render(context))


def my_waiting_offers(request):
    offers_set = ServiceOffer.find_all_waiting_for_response(user_id=request.user.id)

    paginator = Paginator(offers_set, 10)
    page = request.GET.get('page')

    try:
        offers = paginator.page(page)
    except PageNotAnInteger:
        offers = paginator.page(1)
    except EmptyPage:
        offers = paginator.page(paginator.num_pages)

    template = loader.get_template('offers/my.html')
    context = RequestContext(request, {
        'offers': offers,
    })
    return HttpResponse(template.render(context))

def my_responses(request, status=None):
    responses_set = ServiceOfferResponse.find_all(
        user_id=request.user.id, status=status)

    paginator = Paginator(responses_set, 10)
    page = request.GET.get('page')

    try:
        responses = paginator.page(page)
    except PageNotAnInteger:
        responses = paginator.page(1)
    except EmptyPage:
        responses = paginator.page(paginator.num_pages)

    template = loader.get_template('offers/my_responses.html')
    context = RequestContext(request, {
        'responses': responses,
    })
    return HttpResponse(template.render(context))
