from django.conf.urls import url

from .views.lists import offer_list, my_offers, my_waiting_offers, my_responses, categories
from .views.crud import OfferCreateView, OfferUpdateView, view
from .views.custom import response, up, cancel, restore, accept_response, decline_response

urlpatterns = [
    url(r'^$', categories, name='list_categories'),
    url(r'^offers/search/category/(?P<category>[^?]+)$', offer_list, name='search_category_offers'),
    url(r'^offers/search$', offer_list, name='search_offers'),
    
    url(r'^offers/mine/waiting/$', my_waiting_offers, name='list_my_waiting_offers'),
    url(r'^offers/mine/(?P<status>\w+)/$', my_offers, name='list_my_offers'),
    url(r'^offers/mine$', my_offers, name='list_my_offers'),
    url(r'^offers/responses/(?P<status>\w+)/$', my_responses, name='list_my_responses'),
    url(r'^offers/responses$', my_responses, name='list_my_responses'),

    url(r'^offers/new/', OfferCreateView.as_view(), name='new_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/response/', response, name='response_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/up/', up, name='up_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/cancel/$', cancel, name='cancel_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/restore/$', restore, name='restore_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/accept/(?P<response_id>[0-9]+)/$', accept_response, name='accept_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/decline/(?P<response_id>[0-9]+)/$', decline_response, name='decline_offer'),   
    url(r'^offers/(?P<pk>[0-9]+)/edit/', OfferUpdateView.as_view(), name='edit_offer'),
    url(r'^offers/(?P<offer_id>[0-9]+)/', view, name='view_offer'),
    

    url(r'^offers/(?P<category>[^?]+)/$', offer_list, name='list_offers_by_category'),
]