from barter.offers.models import ServiceOfferCategory


def get_category_tree():
    categories = ServiceOfferCategory.objects.order_by('name').all()
    return __recursive_process__(categories, None)

def __recursive_process__(categories, parent):
    tree = []
    for category in categories:
        if category.parent_id == parent:
            item = {'key': category.name, 'value': category, 'children': []}
            item['children'] = __recursive_process__(categories, category.id)
            tree.append(item)

    return tree
