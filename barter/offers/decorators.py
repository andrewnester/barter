from django.http.response import HttpResponseRedirect
from django.contrib.auth.models import User
from barter.offers.models import ServiceOfferResponse

def only_has_accepted_offers(function):
    def wrap(self, *args, **kwargs):
        
        user = self.request.user
        if not user.id:
            return HttpResponseRedirect('/')
        
        if 'recipients' not in kwargs:
            return HttpResponseRedirect('/')
        
        recipient_username = kwargs['recipients']
        
        recipient = User.objects.filter(username=recipient_username).first()
              
        count_user_offers = ServiceOfferResponse.objects.filter(offer__user__id=user.id, user_id=recipient.id, status=ServiceOfferResponse.ACCEPTED).count()
        count_recipient_offers = ServiceOfferResponse.objects.filter(offer__user__id=recipient.id, user_id=user.id, status=ServiceOfferResponse.ACCEPTED).count()
        
        if count_user_offers == 0 and count_recipient_offers == 0:
            return HttpResponseRedirect('/')
        
        return function(self, *args, **kwargs)
    
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap