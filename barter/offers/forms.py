from django.forms.models import ModelForm
from django.forms.widgets import Textarea, TextInput, HiddenInput
from django.forms.fields import CharField

from barter.forms import DivErrorListMixin

from .models import ServiceOffer, ServiceOfferResponse


class ServiceOfferForm(DivErrorListMixin, ModelForm):
    short = CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Name'}))
    offer = CharField(widget=Textarea(attrs={'class':'form-control', 'placeholder': 'Offer'}))
    lat = CharField(widget=HiddenInput)
    lng = CharField(widget=HiddenInput)
    
    class Meta:
        model = ServiceOffer
        fields = ('category', 'offer', 'short', 'lat', 'lng')
        
class ServiceOfferResponseForm(DivErrorListMixin, ModelForm):
    reply = CharField(required=False, widget=Textarea(attrs={'class':'form-control', 'placeholder': 'I Can Offer You'}))      
    
    class Meta:
        model = ServiceOfferResponse
        fields = ('reply',)
        