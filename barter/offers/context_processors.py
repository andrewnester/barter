from barter.offers.models import ServiceOfferResponse


def notifications(request):
    offers_count = ServiceOfferResponse.objects.filter(offer__user_id=request.user.id).filter(
        status=ServiceOfferResponse.WAITING).count()
    
    responses_count = ServiceOfferResponse.objects.filter(user_id=request.user.id).filter(
        status=ServiceOfferResponse.WAITING).count()

    return {
        'offers_notifications_count': offers_count,
        'responses_notifications_count': responses_count
    }
