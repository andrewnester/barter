from django.test import TestCase
from django.test import Client

class OffersTestCase(TestCase):

    def test_home_page(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(200, response.status_code)
