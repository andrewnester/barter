from django.forms.widgets import Widget
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

class SearchFieldWidget(Widget):
    html = """
        <form class="navbar-form" action="%s" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="term" value="%s" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
    """
    
    def render(self, name, value='', attrs=None):
        return mark_safe(self.html % (reverse('search_offers'), value))