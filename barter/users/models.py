import datetime

from django.db import models
from django.contrib.auth.models import User
from registration.signals import user_registered
try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now

class Profile(models.Model):
    user = models.OneToOneField(User)
    age = models.IntegerField(null=True)
    phone = models.CharField(max_length=255, default='')
    image = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, default='')
    city =  models.CharField(max_length=255, default='')    
    
    def __unicode__(self):
        return self.user
 
def user_registered_callback(sender, user, request, **kwargs):
    profile = Profile(user = user)
    profile.save()

 
user_registered.connect(user_registered_callback)