from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist

def need_filled_account(function):
    def wrap(request, *args, **kwargs):
        if not request.user.id:
            return HttpResponseRedirect('/accounts/login/')
        try:
            profile = request.user.profile
        except ObjectDoesNotExist:
            profile = False
            
        if profile and profile.email and request.user.first_name and request.user.last_name:
            return function(request, *args, **kwargs)
        else:
            messages.warning(request, _('You need to fill your profile at first'))
            return HttpResponseRedirect('/accounts/edit/?redirect_url=' + request.path)
    
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def generic_need_filled_account(function):
    def wrap(obj, request, *args, **kwargs):
        if not request.user.id:
            return HttpResponseRedirect('/accounts/login/')
        try:
            profile = request.user.profile
        except ObjectDoesNotExist:
            profile = False
            
        if profile and profile.email and request.user.first_name and request.user.last_name:
            return function(obj, request, *args, **kwargs)
        else:
            messages.warning(request, _('You need to fill your profile at first'))
            return HttpResponseRedirect('/accounts/edit/?redirect_url=' + request.path)
    
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def generic_only_author(model_class):
    def only_author_function(function):
        def wrap(obj, request, *args, **kwargs):
            if not request.user.id:
                return HttpResponseRedirect('/accounts/login/')
            
            item = model_class.objects.filter(id=obj.kwargs['pk'], user_id=request.user.id).first()   
                
            if item is not None:
                return function(obj, request, *args, **kwargs)
            else:
                return HttpResponseRedirect('/')
        
        wrap.__doc__ = function.__doc__
        wrap.__name__ = function.__name__
        return wrap
    return only_author_function

def only_author(model_class):
    def only_author_function(function):
        def wrap(request, offer_id, *args, **kwargs):
            if not request.user.id:
                return HttpResponseRedirect('/accounts/login/')

            item = model_class.objects.filter(id=offer_id, user_id=request.user.id).first()   
                
            if item is not None:
                return function(request, offer_id, *args, **kwargs)
            else:
                return HttpResponseRedirect('/')
        
        wrap.__doc__ = function.__doc__
        wrap.__name__ = function.__name__
        return wrap
    return only_author_function