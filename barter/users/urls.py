from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^profile', views.profile, name='profile'),
    url(r'^edit', views.edit, name='edit_profile'),
    url(r'^save-image', views.save_image, name='save_image'),
    url(r'^save', views.save, name='save_profile'),
]