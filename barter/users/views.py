import json
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import login as base_login
from django.contrib.auth.models import User

from barter.users.forms import ImageUploadForm, UserForm, ProfileForm
from .models import Profile

def edit(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/login')
    
    user = User.objects.filter(id=request.user.id).first()
    profile = Profile.objects.filter(user_id=request.user.id).first()
    profile = Profile() if profile is None else profile 
    
    image_form = ImageUploadForm(user=request.user, profile=profile)
    form = UserForm(user.__dict__)
    profile_form = ProfileForm(profile.__dict__)
    
    template = loader.get_template('users/edit_profile.html')
    context = RequestContext(request, {
        'profile': user,
        'image_form': image_form,
        'form': form,
        'profile_form': profile_form,
        'redirect_url': request.GET.get('redirect_url', '/accounts/profile')
    })
    return HttpResponse(template.render(context))

def save(request):
    
    form = UserForm(request.POST)
    profile_form = ProfileForm(request.POST)
    
    profile = Profile.objects.filter(user_id=request.user.id).first()
    profile = Profile() if profile is None else profile 
    
    if not form.is_valid() or not profile_form.is_valid():
        image_form = ImageUploadForm(user=request.user, profile=profile)
        template = loader.get_template('users/edit_profile.html')
        context = RequestContext(request, {
            'profile': request.user,
            'image_form': image_form,
            'form': form,
            'profile_form': profile_form,
            'redirect_url': request.POST.get('redirect_url', '/accounts/profile')
        })
        return HttpResponse(template.render(context))
    
    request.user.first_name = form.cleaned_data['first_name']
    request.user.last_name = form.cleaned_data['last_name']
    request.user.save()
    
    profile.user = request.user
    profile.age = profile_form.cleaned_data['age']
    profile.city = profile_form.cleaned_data['city']
    profile.email = profile_form.cleaned_data['email']
    profile.phone = profile_form.cleaned_data['phone']
    profile.save()
    
    return HttpResponseRedirect(request.POST.get('redirect_url', '/accounts/profile'))

def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    
    if request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/profile')
    
    return base_login(request, template_name=template_name,
          redirect_field_name=redirect_field_name,
          authentication_form=authentication_form,
          current_app=current_app, extra_context=extra_context)

def profile(request):
    
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/login')
    
    profile = Profile.objects.filter(user_id=request.user.id).first()
    profile = Profile() if profile is None else profile 
    
    template = loader.get_template('users/profile.html')
    context = RequestContext(request, {
        'user': request.user,
        'profile': profile
    })
    return HttpResponse(template.render(context))

def save_image(request):
    
    profile_id = request.POST.get('profile', 0)
    profile = Profile.objects.filter(id=profile_id).first();
    
    if profile is None:
        profile = Profile(image=request.POST.get('image'), user_id=request.POST.get('user'))
    else:
        profile.image = request.POST.get('image')
        
    profile.save()
    
    return HttpResponse(json.dumps({'result' : 'ok'}), content_type="application/json")