from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.forms.utils import ErrorList
from django.utils.translation import ugettext_lazy as _
from django.forms.models import ModelForm
from django.forms.fields import EmailField, CharField, IntegerField
from django.forms.widgets import TextInput

from barter.uploader.widgets import ImageUploadWidget

from .models import Profile


class DivErrorList(ErrorList):
    def __unicode__(self):
        return self.as_divs()
    
    def as_divs(self):
        if not self: return u''
        return u'<div class="text-danger text-center">%s</div>' % ''.join([u'<div class="error">%s</div>' % e for e in self])



class AuthForm(AuthenticationForm):
    
    username = forms.CharField(max_length=254, widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'class':'form-control'}))
    
    def __init__(self, *args, **kwargs):
        kwargs_new = {'error_class': DivErrorList}
        kwargs_new.update(kwargs)
        super(AuthForm, self).__init__(*args, **kwargs_new)


class UserForm(ModelForm):
    first_name = CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'First Name'}))
    last_name = CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Last Name'}))
    
    class Meta:
        model = User
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        kwargs_new = {'error_class': DivErrorList}
        kwargs_new.update(kwargs)
        super(UserForm, self).__init__(*args, **kwargs_new)  

class ProfileForm(ModelForm):
    
    email = EmailField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Email'}))
    phone = CharField(required=False, widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Phone'}))
    age = IntegerField(required=False, max_value=100, min_value=10, widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Age'}))
    city = CharField(required=False, widget=TextInput(attrs={'class':'form-control', 'placeholder': 'City'}))
    
    
    class Meta:
        model = Profile
        fields = ('email', 'phone', 'age', 'city')
    
    def __init__(self, *args, **kwargs):
        kwargs_new = {'error_class': DivErrorList}
        kwargs_new.update(kwargs)
        super(ProfileForm, self).__init__(*args, **kwargs_new)  
           
    
    
class ImageUploadForm(forms.Form):
    image = forms.URLField(widget=ImageUploadWidget(upload_to='uploads'))
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user');
        profile = kwargs.pop('profile');
        super(ImageUploadForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.user = user;
        self.fields['image'].widget.profile = profile;
        
    