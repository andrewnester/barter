from barter import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from barter.offers.models import ServiceOffer, ServiceOfferResponse
from barter.services import JobManager

@receiver(post_save, sender=ServiceOffer)
def new_offer_email(sender, created, **kwargs):
    if not created:
        return

    job_manager = JobManager()
    job_manager.set_send_email_task(subject="Barter.by - New service offer created",
                                    msg="Service Offer created",
                                    emails=settings.NOTIFICATION_EMAIL_ADDRESSES)

