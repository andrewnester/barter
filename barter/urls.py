import barter

from django.conf.urls import include, url
from django.contrib import admin

from users.forms import AuthForm
from registration.backends.default.views import RegistrationView
from registration.forms import RegistrationFormUniqueEmail

from barter.users import views


SOCIAL_AUTH_URL_NAMESPACE = 'social'

urlpatterns = [
                              
    url(r'^social/', include('social.apps.django_app.urls', namespace='social')),

    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^messages/', include('barter.privatemsgs.urls')),
        
    url(r'accounts/register/$', 
        RegistrationView.as_view(form_class = RegistrationFormUniqueEmail), 
        name = 'registration_register'),
    
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='registration_logout'),

    url(r'^accounts/login/$', views.login, {'authentication_form' : AuthForm}, name='login'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/', include('barter.users.urls')),

    url(r'^uploader/', include('barter.uploader.urls')),
    
    url(r'^media/(?P<path>.*)', 'django.views.static.serve', {'document_root': barter.settings.MEDIA_ROOT}),
    
    url(r'^', include('barter.offers.urls')), 
    
]
