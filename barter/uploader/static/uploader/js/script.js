var cropWidth = 105;
var cropHeight = 105;
var zoom = 1;

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function ajaxSubmit(){
    $('#modal-photo').modal('hide');
	var csrfToken = getCookie('csrftoken');
	var formData = new FormData();
	formData.append("file", document.getElementById('image_file').files[0]);
	formData.append("x1", $('#x1').val());
	formData.append("x2", $('#x2').val());
	formData.append("y1", $('#y1').val());
	formData.append("y2", $('#y2').val());
	formData.append("w", $('#w').val());
	formData.append("h", $('#h').val());
	
	var request = new XMLHttpRequest();
	request.open("POST", "/uploader/upload");
	request.setRequestHeader("X-CSRFToken", csrfToken);
	request.send(formData);
	
	request.onreadystatechange=function(){
       if (request.readyState == 4 && request.status == 200){

          var data = $.parseJSON(request.responseText);
          if (typeof jcrop_api != 'undefined') {
			jcrop_api.destroy();
			jcrop_api = null;
			
			var oImage = document.getElementById('avatar');
			oImage.onload = function(){
                $('#upload-button').hide();

                var formData = new FormData();
                formData.append('image', data['url']);
                formData.append('user', $('#user_id').val());
                formData.append('profile', $('#profile').val());
                
                var request = new XMLHttpRequest();
            	request.open("POST", "/accounts/save-image");
				request.setRequestHeader("X-CSRFToken", csrfToken);
				request.send(formData);

			};
			oImage.src = data['url'];
           }
       }
    }
	
}

// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
function checkForm() {
    if (parseInt($('#w').val())) return true;
    $('.error').html('Please select a crop region and then press Upload').show();
    return false;
};

// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    $('#x1').val(e.x * zoom);
    $('#y1').val(e.y * zoom);
    $('#x2').val(e.x2 * zoom);
    $('#y2').val(e.y2 * zoom);
    $('#w').val(e.w * zoom);
    $('#h').val(e.h * zoom);
};


// Create variables (in this scope) to hold the Jcrop API and image size
var jcrop_api, boundx, boundy;

function fileSelectHandler() {

    // get selected file
    var oFile = $('#image_file')[0].files[0];
	if(!oFile){
		$('#preview').css({width:0, height:0});
		$('#upload-button').hide();
		if (jcrop_api != undefined && jcrop_api != null) {
			jcrop_api.destroy();
			jcrop_api = null;
        }
        return;
	}

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    // check for file size
    if (oFile.size > 250 * 1024) {
        $('.error').html('You have selected too big file, please select a one smaller image file').show();
        return;
    }

    // preview element
    var oImage = document.getElementById('preview');
 	oImage.onload = function () { // onload event handler
        
        $('#modal-photo').modal('show');

        if (jcrop_api != undefined && jcrop_api != null) {
			jcrop_api.destroy();
			jcrop_api = null;
        }
                
        $('#preview').width(oImage.naturalWidth / zoom);
        $('#preview').height(oImage.naturalHeight / zoom);        

        $('#preview').Jcrop({
        	allowSelect: false,
            minSize: [32, 32], // min crop size
            aspectRatio : 1, // keep aspect ratio 1:1
            bgFade: true, // use fade effect
            bgOpacity: .3, // fade opacity
            boxWidth: 550,
            onChange: updateInfo,
            onSelect: updateInfo,
        }, function(){

            // use the Jcrop API to get the real image size
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];

            // Store the Jcrop API in the jcrop_api variable
            jcrop_api = this;
            
            this.animateTo([0,0,cropWidth / zoom, cropHeight / zoom]);

		    // you can also set selection area without the fancy animation
		    this.setSelect([0,0,cropWidth / zoom, cropHeight / zoom]);
            
           	$('#upload-button').show();
            
        });

    };



    var oReader = new FileReader();
    oReader.onload = function(e) {
        oImage.src = e.target.result;
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}

function setZoom(oImage){
	if(oImage.naturalWidth >= 1600){
		zoom = 18;
		return;
	}
	
	if(oImage.naturalWidth >= 800){
		zoom = 2;
		return;
	}
	
	zoom = 1;
	
}
