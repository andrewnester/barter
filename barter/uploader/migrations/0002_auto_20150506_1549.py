# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachmentmodel',
            name='offer',
            field=models.ForeignKey(default=None, blank=True, to='offers.ServiceOffer', null=True),
        ),
    ]
