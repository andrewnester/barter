# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0002_auto_20150506_1549'),
    ]

    operations = [
        migrations.AddField(
            model_name='attachmentmodel',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 6, 16, 16, 36, 891350, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
