# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0014_serviceoffer_update_time'),
        ('uploader', '0003_attachmentmodel_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'')),
                ('created', models.DateTimeField(auto_now=True)),
                ('offer', models.ForeignKey(default=None, blank=True, to='offers.ServiceOffer', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='attachmentmodel',
            name='offer',
        ),
        migrations.DeleteModel(
            name='AttachmentModel',
        ),
    ]
