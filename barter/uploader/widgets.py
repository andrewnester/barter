from django.forms import widgets
from django.utils.safestring import mark_safe


class ImageUploadWidget(widgets.FileInput):
    
    html = """
    <div class="error"></div>
    <p><img id="avatar" style="width:100px" src="%s"/></p>
    <form id="form" action="/accounts/save"> 
        <span class="btn btn-xs btn-primary btn-file">   
            Choose Photo <input id="image_file" type="file" name="file" onchange="fileSelectHandler()">
        </span>
        
        <input type="hidden" id="profile" value="%s"/>
        <input type="hidden" id="user_id" value="%s"/>
        
        <input type="hidden" id="x1"/>
        <input type="hidden" id="x2"/>
        <input type="hidden" id="y1"/>
        <input type="hidden" id="y2"/>
        <input type="hidden" id="w"/>
        <input type="hidden" id="h"/>
    </form>
    """
    
    class Media:

        js = [
            "uploader/js/jquery-1.11.2.min.js",
            "uploader/js/jquery.Jcrop.min.js",
            "uploader/js/script.js"
        ]
        
        css = {
            "all": [
                "uploader/css/jquery.Jcrop.min.css"
            ]
        }
        
    def __init__(self, *args, **kwargs):
        self.upload_to = kwargs.pop('upload_to', '')
        self.user = kwargs.pop('user', '')
        super(widgets.FileInput, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        return mark_safe(self.html % ( 
                                      self.profile.image if self.profile.image else "http://placehold.it/105x105", 
                                      self.profile.id if self.profile.id is not None else 0,
                                      self.user.id
                                    )
                         )