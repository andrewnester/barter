from django.db import models
from barter.offers.models import ServiceOffer

class Attachment(models.Model):
    offer = models.ForeignKey(ServiceOffer, null=True, blank=True, default = None)
    file = models.FileField()
    created = models.DateTimeField(auto_now = True)
