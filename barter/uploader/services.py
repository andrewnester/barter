from PIL import Image, ImageOps
try:
    from StringIO import StringIO as IO
except ImportError:
    from io import BytesIO as IO

from django.core.files.uploadedfile import SimpleUploadedFile


def resize(file_, x1 = 0, x2 = 0, y1 = 0, y2 = 0, w = 0, h = 0):
    if(w is 0 and h is 0):
        return file_

    image = Image.open(file_)

    if(image.mode == 'RGBA'):
        image.load()
        background = Image.new('RGB', image.size, (255, 255, 255))
        background.paste(image, mask=image.split()[3])
        image = background

    temp = IO()

    image = image.crop((x1, y1, x2, y2));

    image.save(temp, 'jpeg')
    temp.seek(0)

    return SimpleUploadedFile(file_.name,
                              temp.read(),
                              content_type='image/jpeg')