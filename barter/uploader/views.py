import os, json
from django.views.decorators.http import require_POST
from django.http.response import HttpResponse
from django.utils.text import slugify
from django.conf import settings
from django.core.files.storage import default_storage

from .forms import FileForm
from .services import resize

UPLOAD_PATH = getattr(settings, 'UPLOADER_DIR', 'uploads/')

@require_POST
def upload(request):
    form = FileForm(request.POST, request.FILES)
    if form.is_valid():
        file_ = form.cleaned_data['file']

        image_types = ['image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg',
                       'image/gif']

        if file_.content_type not in image_types:
            data = json.dumps({'error': 'Bad image format.'})
            return HttpResponse(data, content_type="application/json", status=403)

        file_name, extension = os.path.splitext(file_.name)
        safe_name = '{0}{1}'.format(slugify(file_name), extension)

        x1 = int(round(float(request.POST.get('x1', 0))));
        x2 = int(round(float(request.POST.get('x2', 0))));
        y1 = int(round(float(request.POST.get('y1', 0))));
        y2 = int(round(float(request.POST.get('y2', 0))));
        w = int(round(float(request.POST.get('w', 0))));
        h = int(round(float(request.POST.get('h', 0))));

        if w is not 0 and h is not 0:
            file_ = resize(file_, x1, x2, y1, y2, w, h)

        name = os.path.join(UPLOAD_PATH, safe_name)
        path = default_storage.save(name, file_)
        url = default_storage.url(path)

        return HttpResponse(json.dumps({'url': url, 'filename': path}), content_type="application/json")

    return HttpResponse(status=403)