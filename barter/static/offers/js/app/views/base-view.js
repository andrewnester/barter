/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function($) {
    'use strict';

    var TemplateManager = {
        templates: {},

        get: function(id, callback) {
            var template = this.templates[id];
            if (template) {
                callback(template);

            } else {
                var that = this;
                $.get("/static/templates/" + id + ".html", function(template){
                    var tmpl = template;
                    that.templates[id] = tmpl;
                    callback(tmpl);
                });
            }

        }

    }

    // Our overall **AppView** is the top-level piece of UI.
    app.BaseView = Backbone.View.extend({
        render: function() {
            var that = this;
            TemplateManager.get(this.template, function(template) {
                var html = _.template(template, that.model.toJSON);
                that.$el.html(html);
            });
            return this;
        }
    });
})(jQuery);
