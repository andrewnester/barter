/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {
	'use strict';


	// The Application
	// ---------------

	// Our overall **AppView** is the top-level piece of UI.
	app.AppView = app.BaseView.extend({

		// Instead of generating a new element, bind to the existing skeleton of
		// the App already present in the HTML.
		el: '#offers',

		// Our template for the line of statistics at the bottom of the app.
		template: 'offers',

		// Delegated events for creating new items, and clearing completed ones.
		events: {
		},


		// At initialization we bind to the relevant events on the `Todos`
		// collection, when items are added or changed. Kick things off by
		// loading any preexisting todos that might be saved in *localStorage*.
		initialize: function () {

			this.$offers = $('#offer-list');

			this.listenTo(app.offers, 'add', this.addOne);
			this.listenTo(app.offers, 'reset', this.addAll);

			app.offers.fetch({reset: true});
		},

		// Add a single todo item to the list by creating a view for it, and
		// appending its element to the `<ul>`.
		addOne: function (offer) {
			var view = new app.OfferView({ model: offer });
			this.$offers.append(view.render().el);
		},

		// Add all items in the **Todos** collection at once.
		addAll: function () {
			this.$offers.removeClass('loader');
			this.$offers.html('');
			app.offers.each(this.addOne, this);
		},

	});
})(jQuery);