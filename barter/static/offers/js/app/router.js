/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	var ItemsRouter = Backbone.Router.extend({
	});

	app.ItemsRouter = new ItemsRouter();
	Backbone.history.start();
})();