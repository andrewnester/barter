var map;
function initializeMap(edit, lat, lng) {

	var edit = edit || false
	var lat = lat || 53.9
	var lng = lng || 27.566
	
	function updateFormData(location){		
		$('input[name="lat"]').val(location.lat())
		$('input[name="lng"]').val(location.lng())
	}	
	
	function moveMarker(location) {
		map.setCenter(location);
		marker.setPosition(location)
	
		updateFormData(location)
	}

	var minsk = new google.maps.LatLng(lat, lng)
	var mapOptions = {
		zoom : 6,
		center : minsk
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	var marker = new google.maps.Marker({
		draggable : edit ? true: false,
		animation : google.maps.Animation.DROP,
		map : map
	});
	
	moveMarker(minsk);

	if(edit){
		google.maps.event.addListener(map, 'click', function(event) {
			moveMarker(event.latLng);
		});
		
		google.maps.event.addListener(marker, 'dragend', function(event) { 
			updateFormData(event.latLng)
		});
	}	

}

