/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	// Todo Collection
	// ---------------

	// The collection of todos is backed by *localStorage* instead of a remote
	// server.
	var Offers = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: app.Offer,
		url: '/api/offers',

	});

	// Create our global collection of **Todos**.
	app.offers = new Offers();
})();