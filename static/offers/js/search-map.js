var map;
function initializeMap(leftTop, rightBottom, zoom) {

	if(leftTop && rightBottom){
		var leftTopBounds = leftTop.split(';');
		var rightBottomBounds = rightBottom.split(';');

		var filterLng = (parseFloat(rightBottomBounds[0]) + parseFloat(leftTopBounds[0])) / 2;
		var filterLat = (parseFloat(rightBottomBounds[1]) + parseFloat(leftTopBounds[1])) / 2;
	}

	var lat = filterLat || 53.9
	var lng = filterLng || 27.566
	var zoom = parseInt(zoom) || 10;

	var minsk = new google.maps.LatLng(lat, lng)
	var mapOptions = {
		zoom : zoom,
		center : minsk
	};
	map = new google.maps.Map(document.getElementById('filter-map'), mapOptions);

	google.maps.event.addListener(map, 'bounds_changed', function(){
		storeMap(map.getBounds(), map.getZoom());
	});

	function storeMap(bounds, zoom){
		$('#filter-left-top').val(bounds.qa.A + ';'+bounds.za.A);
		$('#filter-right-bottom').val(bounds.qa.j + ';'+bounds.za.j);
		$('#filter-zoom').val(zoom);
	}
}


function addMarker(name, lat, lng){
	var marker = new google.maps.Marker({
      position: new google.maps.LatLng(parseFloat(lat.replace(",", ".")), parseFloat(lng.replace(",", "."))),
      map: map,
      title: name
  });
}
